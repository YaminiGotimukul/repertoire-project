
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.png">

<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

<!-- Bootstrap theme -->
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">
<!-- Custom styles for this template -->
<script
	src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
<style type="text/css">
h1 {
	color: #006699;
	padding-left: 15px;
}
</style>
<title>Repertoire</title>
</head>
<body>
<form action="" method="post">
	<table  >
		<tr>
			<td style="width:80%">
				<div class="page-header" style="margin: 20px 0px 0px 0px;">
					<h1><i>Repertoire</i></h1>
				</div>
			</td>
			<td style="width:20%">
				<div class="input-prepend">
    				<span class="add-on"><i class="icon-search"></i></span>
    				<input class="span2" type="text" placeholder="Search">
  				</div>
  				<div style="margin-left: 20px; margin-top:10px;">
						<a class="btn btn-default" href="#">
  						<i class="icon-backward icon-large"></i> Back</a>
				</div>
  				
			</td>
		</tr>
	</table>
	
	
		<table style="width: 1000px;" >
			<tr>
				<td>
					<div class="span3">
						<div class="well" style="padding: 8px 0; width: 200px;">
							<ul class="nav nav-list"><li class="active"><a href="home"><i class="icon-fixed-width icon-home"></i> Home</a></li>
								<li><a href="freeBSD"><i class="icon-fixed-width icon-pencil"></i> Free BSD</a></li>
								<li><a href="openBSD"><i class="icon-fixed-width icon-pencil"></i> Open BSD</a></li>
								<li><a href="netBSD"><i class="icon-fixed-width icon-pencil"></i> Net BSD</a></li>
								<li><a href="notifyEmail"><i class="icon-fixed-width icon-exclamation-sign"></i> Notification</a></li>
								<li><a href="repertoireContent"><i class="icon-fixed-width icon-book"></i> About Repertoire</a></li>
							</ul>
						</div>
					</div>
				</td>
				
				<td>
					
				<div class="col-sm-4">
				<div class="panel panel-info" style="width: 900px; margin: 20px 0px 0px 0px">
				<div class="panel-heading">
					<h3 class="panel-title">Compare Page</h3>
				</div>
				<div class="panel-body">
					<table style="width:850px;"border="1">
						<tr>
							<td style="width:400px">
								<font size="4"><b>Net BSD</b></font>
							</td>
							<td style="width:400px"> 
								<font size="4"><b>Free BSD</b></font>
  							</td>
						</tr>
						<tr style="height:200px">
							<td style="width:400px ">
								<font size="3"><b>Ported edits in comparison </b></font><br/><br/>
								<font size="3"><b>File names of Net BSD family that are similar to Free BSD  </b></font><br/>
								<a href="http://cvsweb.netbsd.org/bsdweb.cgi/src/usr.sbin/makefs/makefs.c?only_with_tag=MAIN"><%=request.getAttribute("firstColumn") %></a><br/><br/>
								
							</td>
							<td style="width:400px">
								<font size="3"><b>Ported edits in comparison </b></font><br/><br/><br/><br/>
								<a href="">Coo.c of Project Course</a><br/><br/>
							</td>
						</tr>
					
						
					</table>
					</div><br/><br/>
  				</div>
			</div><!-- /.col-sm-4 -->
			</td>
			</tr>
			<tr height="15px;">
							<td colspan="2" style="width: 500px;" />
			</tr>
			<tr>
				<td></td>
				<td>
					
				</td>
			</tr>
		</table>
</form>
</body>
</html>