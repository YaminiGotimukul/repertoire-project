
<!DOCTYPE html >

<html>
<head>
<meta charset= "UTF-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.png">

<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

<!-- Bootstrap theme -->
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">
<!-- Custom styles for this template -->
<script
	src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">


<style type="text/css">
h1 {
	color: #006699;
	padding-left: 15px;
}
</style>

<script type="text/javascript">
    	function onSubmit(button){
    		document.forms[0].button.value = button;
    		document.forms[0].submit();
    	}
    
</script>
<title>Repertoire</title>
</head>

<body>
<form action="home" method="POST">
	<table >
		<tr>
			<td style="width:80%">
				<div class="page-header" style="margin: 20px 0px 0px 0px;">
					<h1><i>Repertoire</i></h1>
				</div>
			</td>
			<td style="width:20%">
				<div class="input-prepend">
    				<span class="add-on"><i class="icon-search"></i></span>
    				<input class="span2" type="text" placeholder="Search">
  				</div>
  				<div style="margin-left: 20px; margin-top:10px;">
						<a class="btn btn-default" href="#">
  						<i class="icon-backward icon-large"></i> Back</a>
				</div>
  				
			</td>
		</tr>
	</table>
	
	
		<table style="width: 1000px;">
			<tr>
				<td>
					<div class="span3">
						<div class="well" style="padding: 8px 0; width: 200px;">
							<ul class="nav nav-list"><li class="active"><a href="home"><i class="icon-fixed-width icon-home"></i> Home</a></li>
								<li><a href="freeBSD"><i class="icon-fixed-width icon-pencil"></i> Free BSD</a></li>
								<li><a href="openBSD"><i class="icon-fixed-width icon-pencil"></i> Open BSD</a></li>
								<li><a href="netBSD"><i class="icon-fixed-width icon-pencil"></i> Net BSD</a></li>
								<li><a href="notifyEmail"><i class="icon-fixed-width icon-exclamation-sign"></i> Notification</a></li>
								<li><a href="repertoireContent"><i class="icon-fixed-width icon-book"></i> About Repertoire</a></li>
							</ul>
						</div>
					</div>
				</td>
				
				<td>
					
					<div class="col-sm-4">
					<div class="panel panel-success" style="width: 800px; margin: 20px 0px 0px 0px">
						<div class="panel-heading">
							<h3 class="panel-title">Home Page</h3>
						</div>
						<div class="panel-body">
							<table style="width:700px;">
								<tr>
									<td style="width:300px">
										<font size="3">Fill in this form to compare history of different projects</font>
									</td>
									<td style="width:400px"> 
								
  									</td>
								</tr>
								
								<tr height="15px;">
									<td colspan="2" style="width: 300px;"></td>
								</tr>
								
								<tr>
									<td style="width:300px ">
										<font size="3">Date Range  </font>
									</td>
									<td style="width:400px">
										<font size="3">From Date:</font>
											<div class="input-prepend">
    										<span class="add-on"><i class="icon-calendar"></i></span>
    											<input class="span2" name="fromDate" type="text" placeholder="Ex From:10/12/2011 ">
  											</div>&nbsp;&nbsp;&nbsp;&nbsp;<font size="3">To Date:</font>
  											<div class="input-prepend">
    										<span class="add-on"><i class="icon-calendar"></i></span>
    											<input class="span2" name="toDate" type="text" placeholder="Ex to:12/12/2011">
  											</div>
									</td>
								</tr>
								
								<tr height="15px;">
									<td colspan="2" style="width: 300px;"></td>
								</tr>
								
								<tr>
									<td style="width:300px ">
										<font size="3">Type of edits</font>
									</td >
									<td style ="width:400px">
										<input type="checkbox" name="editTypes" value="" checked>Ported Edits &nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="editTypes" value="" > All edits &nbsp;&nbsp;&nbsp;&nbsp;
									</td>
								</tr>
								
								<tr height="15px;">
									<td colspan="2" style="width: 300px;" />
								</tr>
								
								<tr>
									<td style="width:300px ">
										<font size="3">Compare Repository</font>
									</td>
									<td style="width:400px">
										<input type="checkbox" name="repositorySubscribed" value="" checked> Free BSD &nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="repositorySubscribed" value="" > Open BSD &nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="repositorySubscribed" value="" > Net BSD
									</td>
								</tr>
						
								<tr>
									<td style="width:300px ">
									</td >
									<td style ="width:400px">
									</td>
								</tr>
						</table>
						
					</div><br/><br/>
  				</div>
			</div><!-- /.col-sm-4 -->
		</td>
	</tr>
	<tr height="15px;">
		<td colspan="2" style="width: 500px;" />
	</tr>
	<tr>
		<td></td>
		<td>
			<div style="margin-left: 20px;">
						<a class="btn btn-success" href="javascript:onSubmit('Compare');">
  						<i class="icon-envelope icon-large"></i> Compare Repository</a>
			</div>
		</td>
	</tr>
</table>
<input type="hidden" name="button" value="">
</form>
</body>
</html>