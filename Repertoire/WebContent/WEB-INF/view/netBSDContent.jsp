<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.png">

<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

<!-- Bootstrap theme -->
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">
<!-- Custom styles for this template -->
<script
	src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
<style type="text/css">
h1 {
	color: #006699;
	padding-left: 15px;
}
</style>
<title>Repertoire</title>
</head>
<body>
	<form action="" method="post">
		<table>
			<tr>
				<td style="width: 80%">
					<div class="page-header" style="margin: 20px 0px 0px 0px;">
						<h1>
							<i>Repertoire</i>
						</h1>
					</div>
				</td>
				<td style="width: 20%">
					<div class="input-prepend">
						<span class="add-on"><i class="icon-search"></i></span> <input
							class="span2" type="text" placeholder="Search">
					</div>
					<div style="margin-left: 20px; margin-top: 10px;">
						<a class="btn btn-default" href="#"> <i
							class="icon-backward icon-large"></i> Back
						</a>
					</div>

				</td>
			</tr>
		</table>


		<table style="width: 1000px;">
			<tr>
				<td>
					<div class="span3">
						<div class="well" style="padding: 8px 0; width: 200px;">
							<ul class="nav nav-list">
								<li ><a href="home"><i
										class="icon-fixed-width icon-home"></i> Home</a></li>
								<li><a href="freeBSD"><i
										class="icon-fixed-width icon-pencil"></i> Free BSD</a></li>
								<li><a href="openBSD"><i class="icon-fixed-width icon-pencil"></i>
										Open BSD</a></li>
								<li class="active"><a href="netBSD"><i class="icon-fixed-width icon-pencil"></i>
										Net BSD</a></li>
								<li ><a href="notifyEmail"><i
										class="icon-fixed-width icon-exclamation-sign"></i>
										Notification</a></li>
								<li><a href="repertoireContent"><i class="icon-fixed-width icon-book"></i>
										About Repertoire</a></li>
							</ul>
						</div>
					</div>
				</td>

				<td>

					<div class="col-sm-4">
						<div class="panel panel-success"
							style="width: 800px; margin: 20px 0px 0px 0px">
							<div class="panel-heading">
								<h3 class="panel-title">About Net BSD</h3>
							</div>
							<div class="panel-body">
								<table style="width: 700px;">
									<tr>
										<td style="width: 700px">
											<p>
												<font size="3">NetBSD focuses on clean design and
													well architected solutions. Because of this NetBSD may
													support certain 'exciting' features later than other
													systems, but as time progresses the NetBSD codebase is
													getting even stronger and easier to manage, while other
													systems that value features over code quality are finding
													increasing problems with code management and conflicts.

													NetBSD supports a massive range of hardware platforms from
													a single source tree, including simultaneous release across
													all platforms, and continues to attract users and
													experienced developers despite lack of media exposure and
													commercial backing - all thanks to attention to code
													quality. </font>
											</p>
											<p>
												<font size="3">BSD Licence (top) While NetBSD uses
													the GNU toolchain (compiler, assembler, etc), and certain
													other GNU tools, the entire kernel and the core of the
													userland utilities are shipped under a BSD licence. This
													allows companies to develop products based on NetBSD
													without the requirement to make changes public (as with the
													GPL). While the NetBSD Project encourages companies and
													individuals to feed back changes to the tree, we respect
													their right to make that decision themselves.</font>
											</p>
										</td>
									</tr>
								</table>
							</div>
							<br /> <br />
						</div>
					</div> <!-- /.col-sm-4 -->
				</td>
			</tr>
		</table>
	</form>
</body>
</html>