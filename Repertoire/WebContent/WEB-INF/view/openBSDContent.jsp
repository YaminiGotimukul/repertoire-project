<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.png">

<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

<!-- Bootstrap theme -->
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">
<!-- Custom styles for this template -->
<script
	src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
<style type="text/css">
h1 {
	color: #006699;
	padding-left: 15px;
}
</style>
<title>Repertoire</title>
</head>
<body>
	<form action="" method="post">
		<table>
			<tr>
				<td style="width: 80%">
					<div class="page-header" style="margin: 20px 0px 0px 0px;">
						<h1>
							<i>Repertoire</i>
						</h1>
					</div>
				</td>
				<td style="width: 20%">
					<div class="input-prepend">
						<span class="add-on"><i class="icon-search"></i></span> <input
							class="span2" type="text" placeholder="Search">
					</div>
					<div style="margin-left: 20px; margin-top: 10px;">
						<a class="btn btn-default" href="#"> <i
							class="icon-backward icon-large"></i> Back
						</a>
					</div>

				</td>
			</tr>
		</table>


		<table style="width: 1000px;">
			<tr>
				<td>
					<div class="span3">
						<div class="well" style="padding: 8px 0; width: 200px;">
							<ul class="nav nav-list">
								<li><a href="home"><i
										class="icon-fixed-width icon-home"></i> Home</a></li>
								<li><a href="freeBSD"><i
										class="icon-fixed-width icon-pencil"></i> Free BSD</a></li>
								<li class="active"><a href="openBSD"><i
										class="icon-fixed-width icon-pencil"></i> Open BSD</a></li>
								<li><a href="netBSD"><i
										class="icon-fixed-width icon-pencil"></i> Net BSD</a></li>
								<li><a href="notifyEmail"><i
										class="icon-fixed-width icon-exclamation-sign"></i>
										Notification</a></li>
								<li><a href="repertoireContent"><i class="icon-fixed-width icon-book"></i>
										About Repertoire</a></li>
							</ul>
						</div>
					</div>
				</td>

				<td>

					<div class="col-sm-4">
						<div class="panel panel-success"
							style="width: 800px; margin: 20px 0px 0px 0px">
							<div class="panel-heading">
								<h3 class="panel-title">About Open BSD</h3>
							</div>
							<div class="panel-body">
								<table style="width: 700px;">
									<tr>
										<td style="width: 700px">
											<p>
												<font size="3">Obviously, each developer working on
													OpenBSD has their own aims and priorities, but it is
													possible to classify the goals we all share: Provide the
													best development platform possible. Provide full source
													access to developers and users, including the ability to
													look at CVS tree changes directly. Users can even look at
													our source tree and changes directly on the web! Integrate
													good code from any source with acceptable copyright (ISC or
													Berkeley style preferred, GPL acceptable as a last recourse
													but not in the kernel, NDA never acceptable). We want to
													make available source code that anyone can use for ANY
													PURPOSE, with no restrictions. We strive to make our
													software robust and secure, and encourage companies to use
													whichever pieces they want to. There are commercial
													spin-offs of OpenBSD. Pay attention to security problems
													and fix them before anyone else does. (Try to be the #1
													most secure operating system.) Greater integration of
													cryptographic software. This means IPsec, key engines,
													Kerberos, and other forms of strong crypto or crypto-using
													systems. OpenBSD is developed and released from Canada and
													due to Canadian law it is legal to export crypto to the
													world (as researched by a Canadian individual and as
													documented in the Export Control list of Canada). OpenBSD
													developers are doing active research and development on
													IPsec. Track and implement standards (ANSI, POSIX, parts of
													X/Open, etc.) Work towards a very machine independent
													source tree. Support as many different systems and hardware
													as feasible. Be as politics-free as possible; solutions
													should be decided on the basis of technical merit. Focus on
													being developer-oriented in all senses, including holding
													developer-only events called hackathons. Do not let serious
													problems sit unsolved. Provide a good cross
													compile/development platform. Import external packages with
													minimal modifications - making upgrading much easier. Also
													to submit back to the developers any changes made. Make a
													CDROM-based release approximately every six months, in
													particular to fund the project...</font>
											</p>
										</td>
									</tr>
								</table>
							</div>
							<br /> <br />
						</div>
					</div> <!-- /.col-sm-4 -->
				</td>
			</tr>
		</table>
	</form>
</body>
</html>