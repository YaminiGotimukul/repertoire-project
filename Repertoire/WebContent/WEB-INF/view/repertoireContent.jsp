<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.png">

<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

<!-- Bootstrap theme -->
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">
<!-- Custom styles for this template -->
<script
	src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
<style type="text/css">
h1 {
	color: #006699;
	padding-left: 15px;
}
</style>
<title>Repertoire</title>
</head>
<body>
	<form action="" method="post">
		<table>
			<tr>
				<td style="width: 80%">
					<div class="page-header" style="margin: 20px 0px 0px 0px;">
						<h1>
							<i>Repertoire</i>
						</h1>
					</div>
				</td>
				<td style="width: 20%">
					<div class="input-prepend">
						<span class="add-on"><i class="icon-search"></i></span> <input
							class="span2" type="text" placeholder="Search">
					</div>
					<div style="margin-left: 20px; margin-top: 10px;">
						<a class="btn btn-default" href="#"> <i
							class="icon-backward icon-large"></i> Back
						</a>
					</div>

				</td>
			</tr>
		</table>


		<table style="width: 1000px;">
			<tr>
				<td>
					<div class="span3">
						<div class="well" style="padding: 8px 0; width: 200px;">
							<ul class="nav nav-list">
								<li><a href="home"><i
										class="icon-fixed-width icon-home"></i> Home</a></li>
								<li ><a href="freeBSD"><i
										class="icon-fixed-width icon-pencil"></i> Free BSD</a></li>
								<li><a href="openBSD"><i
										class="icon-fixed-width icon-pencil"></i> Open BSD</a></li>
								<li><a href="netBSD"><i
										class="icon-fixed-width icon-pencil"></i> Net BSD</a></li>
								<li><a href="notifyEmail"><i
										class="icon-fixed-width icon-exclamation-sign"></i>
										Notification</a></li>
								<li class="active"><a href="repertoireContent"><i class="icon-fixed-width icon-book"></i>
										About Repertoire</a></li>
							</ul>
						</div>
					</div>
				</td>

				<td>

					<div class="col-sm-4">
						<div class="panel panel-success"
							style="width: 800px; margin: 20px 0px 0px 0px">
							<div class="panel-heading">
								<h3 class="panel-title">About Repertoire tool</h3>
							</div>
							<div class="panel-body">
								<table style="width: 700px;">
									<tr>
										<td style="width: 700px">
											<p>
												<font size="3">Repertoire detects repetitive and
													similar edits among a group of program patches and it is
													based on CCFinderX. Thus it can identify ported code from
													the program patches. Repertoire can be used to evaluate
													repetitive work in similar products and our research group
													has used Repertoire to analyze the extent and
													characteristic of cross-system porting in forked projects.
													You can download Repertoire from here and access the
													analysis data that we have made available in public.

													Repertoire is developed and maintained by Baishakhi Ray.
													Please Dr.Miryung Kim and Baishakhi Ray for questions and
													comments. Repertoire has its own tool web site and the
													analysis data on 18 years of the BSD product family is
													available as well:
													http://dolphin.ece.utexas.edu/Repertoire.html</font>
											</p>
											
										</td>
									</tr>
								</table>
							</div>
							<br /> <br />
						</div>
					</div> <!-- /.col-sm-4 -->
				</td>
			</tr>
		</table>
	</form>
</body>
</html>