package edu.utexas.ece.research.repertoire.dao;

import java.sql.DriverManager;
import java.sql.ResultSet;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class DataAccessLayer {
	
	static final String	JDBC_DRIVER	= "com.mysql.jdbc.Driver";
	static final String	DB_URL		= "jdbc:mysql://146.6.53.85/repertoire";

	static final String	USER		= "yamini";
	static final String	PASS		= "yamini";
	
	public static ResultSet dataBaseConnectivity(){
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
				try {
					Class.forName("com.mysql.jdbc.Driver");
					conn = (Connection) DriverManager.getConnection(DB_URL, USER, PASS);

					if (conn != null) {
						System.out.println("Connecting to database...");
					}
					stmt = (Statement) conn.createStatement();
					String sql;
					sql = "SELECT COUNT(*) FROM clone_pair";
					rs = stmt.executeQuery(sql);
					rs.first();
					System.out.println("From Clone pair table value of the first column " + rs.getInt(1));
					
					
				} catch (Exception se) {
					se.printStackTrace();
				}
				return rs;
			
	}

}
