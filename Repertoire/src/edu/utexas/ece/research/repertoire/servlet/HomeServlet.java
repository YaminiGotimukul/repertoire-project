package edu.utexas.ece.research.repertoire.servlet;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.utexas.ece.research.repertoire.dao.DataAccessLayer;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		DataAccessLayer.dataBaseConnectivity();
		String destination = "/WEB-INF/view/homePage.jsp";
		RequestDispatcher requestDispatcher = getServletContext()
				.getRequestDispatcher(destination);
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// Date fromDate = null;
		// Date toDate = null;
		String fDate = request.getParameter("fromDate");
		String tDate = request.getParameter("toDate");
		/*
		 * try { fromDate = (Date) new
		 * SimpleDateFormat("MM/dd/yyyy").parse(fDate); toDate = (Date) new
		 * SimpleDateFormat("MM/dd/yyyy").parse(tDate); } catch (ParseException
		 * e) { // TODO Auto-generated catch block e.printStackTrace(); }
		 */
		// In the following if statment I have changed the fromDate to fDate and
		// toDate to tDate
		if (fDate != null && tDate != null) {

			ResultSet resultSet = DataAccessLayer.dataBaseConnectivity();
			try {
				if (resultSet.first()) {
					System.out.println("Connected to db from Servlet");
					request.setAttribute("firstColumn", resultSet.getInt(1));
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		String destination = "/WEB-INF/view/comparePage.jsp";
		RequestDispatcher requestDispatcher = getServletContext()
				.getRequestDispatcher(destination);
		requestDispatcher.forward(request, response);

	}

}
